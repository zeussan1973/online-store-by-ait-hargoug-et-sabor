<?php
use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

    Route::get(('/'), [HomeController::class,"index"])->name("home.index");
    Route::get(('/about'), [HomeController::class,"about"])->name("home.about");
    Route::get('/products', 'App\Http\Controllers\ProductController@index')->name("product.index");
    Route::get('/products/{id}', 'App\Http\Controllers\ProductController@show')->name("product.show");
    // route ::get(('/'),[Controller ::class,"index"]) ->name("product.index");
    // route :: get(('/show'),[Controller::class,"show"])->name("product.show");
    // Route::get(('/'), [HomeController::class,"index"])->name("home.index");
    // Route::get(('/about'), [HomeController::class,"about"])->name("home.about");
    // route ::get(('/'),[Controller ::class,"index"]) ->name("product.index");
    // route :: get(('/show'),[Controller::class,"show"])->name("product.show");
